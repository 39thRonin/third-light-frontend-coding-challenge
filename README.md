# Third Light Frontend Coding Challenge

## 1 Frontend Installation

Assuming NPM is available, run `npm install` in the `client/` directory to install the dependencies.

## 2 Frontend Production Build

Run `npm run build` in the `client/` directory to build a production version of the frontend client. The build artifacts will be stored in the `client/dist/` directory, which the Fargo server should be setup to serve via <http://localhost:3000/>.

## 3 Backend Server Initiation

Assuming GO is available, run `sudo ./fargo -c ../` in the `server/src/` directory to start the Fargo HTTP server.

## 4 Frontend Usage

Access <http://localhost:3000/> in a web browser to use the client that's powered by the backend server.
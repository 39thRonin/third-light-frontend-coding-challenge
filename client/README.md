# Fargo Frontend Client

This is an Angular-powered frontend client for the HTTP file server, Fargo.

## Development Server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production Build

Run `npm run build` to build a production version of the project. The build artifacts will be stored in the `dist/` directory.

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DirectoryComponent } from './components/directory/directory.component';
import { SearchComponent } from './components/search/search.component';

const routes: Routes = [
  {
    path: '',
    component: DirectoryComponent,
  },
  {
    path: 'directory/:path',
    component: DirectoryComponent,
  },
  {
    path: 'search/:query',
    component: SearchComponent,
  },
  { path: '**', component: DirectoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

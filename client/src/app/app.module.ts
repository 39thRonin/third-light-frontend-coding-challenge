import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DirectoryComponent } from './components/directory/directory.component';
import { FileService } from './services/file.service';
import { HttpClientModule } from '@angular/common/http';
import { FileSizeModule } from 'ngx-filesize';
import { HrefDirective } from './directives/href.directive';
import { NiceDatePipe } from './pipes/nice-date.pipe';
import { FileComponent } from './components/file/file.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './components/search/search.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';

@NgModule({
  declarations: [
    AppComponent,
    DirectoryComponent,
    SearchComponent,
    FileComponent,
    BreadcrumbsComponent,
    HrefDirective,
    NiceDatePipe,
  ],
  exports: [
    HrefDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FileSizeModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [FileComponent],
  providers: [FileService],
  bootstrap: [AppComponent]
})
export class AppModule { }

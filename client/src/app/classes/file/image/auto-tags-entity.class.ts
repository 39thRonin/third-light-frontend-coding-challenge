import { Entity } from '../../entity.class';
import { FileImageAutoTags } from 'src/app/interfaces/file/image/auto-tags.interface';

/**
 * The file image auto tags entity
 */
export class FileImageAutoTagsEntity extends Entity implements FileImageAutoTags {
  color: string;
  tags: string[];
}

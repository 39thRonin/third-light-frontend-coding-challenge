import { Entity } from '../../entity.class';
import { FileImageInfo } from 'src/app/interfaces/file/image/info.interface';
import { FileImageAutoTagsEntity } from './auto-tags-entity.class';
import { FileImageLocationEntity } from './location-entity.class';

/**
 * The file image info entity
 */
export class FileImageInfoEntity extends Entity implements FileImageInfo {
  width: number;
  height: number;
  created: string;
  location: FileImageLocationEntity;
  autotags: FileImageAutoTagsEntity;
}

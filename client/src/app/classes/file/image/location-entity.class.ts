import { Entity } from '../../entity.class';
import { FileImageLocation } from 'src/app/interfaces/file/image/location.interface';

/**
 * The file image location entity
 */
export class FileImageLocationEntity extends Entity implements FileImageLocation {
  lat: number;
  long: number;
}

import { Entity } from '../entity.class';
import { FileInfo } from 'src/app/interfaces/file/info.interface';
import { FileKind } from 'src/app/enums/file-kind.enum';
import { FileImageInfoEntity } from './image/info-entity.class';
import { FileLinksEntity } from './links-entity.class';

/**
 * The file info entity
 */
export class FileInfoEntity extends Entity implements FileInfo {
  name: string;
  size: number;
  modTime: string;
  kind: FileKind;
  filetype: string;
  imageInfo: FileImageInfoEntity;
  exif: object;
  metadata: object;
  path: string;
  links: FileLinksEntity;

  metadataFields: any[] = [];
  metadataArrayFields: any[] = [];

  public get mimeType(): string {
    return `${this.kind}/${this.filetype}`;
  }

  /**
   * Populate the entity using a basic object
   */
  public populateFromObject(entity) {

    super.populateFromObject(entity);

    for (const i in this.metadata) {
      if (this.metadata.hasOwnProperty(i)) {
        if (Array.isArray(this.metadata[i])) {
          let valid = true;
          this.metadata[i].forEach((element) => {
            if (Array.isArray(element) || typeof element === 'object') {
              valid = false;
            }
          });
          if (valid) {
            this.metadataArrayFields.push(i);
          }
        } else if (typeof this.metadata[i] !== 'undefined' && typeof this.metadata[i] !== 'object') {
          this.metadataFields.push(i);
        }
      }
    }
    return this;
  }
}

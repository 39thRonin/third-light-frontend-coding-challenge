import { Entity } from '../entity.class';
import { FileLinks } from 'src/app/interfaces/file/links.interface';

/**
 * The file links entity
 */
export class FileLinksEntity extends Entity implements FileLinks {
  dir: string;
  thumb: string;
  preview: string;
  original: string;
  metadata: string;
}

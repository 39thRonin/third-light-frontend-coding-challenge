import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

/**
 * The directory component
 */
@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
})
export class BreadcrumbsComponent {

  /**
   * The search form
   */
  public searchForm: FormGroup = this.formBuilder.group({
    search: ['', Validators.maxLength(255) ],
  });

  @Input() public query: string = null;

  /**
   * The breadcrumbs, in array form
   */
  @Input() public breadcrumbs: string[] = [];

  /**
   * Constructor
   */
  public constructor(
    private router: Router,
    private formBuilder: FormBuilder,
  ) {}

  public onSubmitSearchForm() {
    this.router.navigate(['/search/' + this.searchForm.get('search').value]);
  }

  public isLastBreadcrumb(i: number) {
    return (i + 1) === this.breadcrumbs.length;
  }

  public getBreadcrumbPath(i: number) {
    return this.breadcrumbs.slice(0, i + 1).join('/');
  }
}

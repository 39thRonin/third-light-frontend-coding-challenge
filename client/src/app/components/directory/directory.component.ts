import { Component, OnInit } from '@angular/core';
import { FileService } from 'src/app/services/file.service';
import { finalize } from 'rxjs/operators';
import { FileInfoEntity } from 'src/app/classes/file/info-entity.class';
import { FileKind } from 'src/app/enums/file-kind.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FileComponent } from '../file/file.component';

/**
 * The directory component
 */
@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
})
export class DirectoryComponent implements OnInit {

  /**
   * The directories to show
   */
  public directories: string [] = [];

  /**
   * The files to show
   */
  public files: FileInfoEntity[] = [];

  /**
   * Allow the FileKind enum to be accessible to the template
   */
  public fileKind = FileKind;

  /**
   * The breadcrumbs, in array form
   */
  public breadcrumbs: string[] = [];

  /**
   * The current path to poke Fargo with
   */
  public currentPath = '';

  private bsModalRef: BsModalRef;

  /**
   * Constructor
   */
  public constructor(
    private fileService: FileService,
    private activatedRoute: ActivatedRoute,
    private modalService: BsModalService,
    private router: Router,
  ) {}

  /**
   * Actions to perform AfterViewInit
   */
  public ngOnInit(): void {
    this.loadDirectory();
  }

  public getDirectoryPath(directory: string) {
    const separator = this.currentPath ? '/' : '';
    return `${this.currentPath}${separator}${directory}`;
  }

  public getFileDownloadPath(file: FileInfoEntity) {
    return `${environment.serverUrl}${file.links.original}`;
  }

  public getThumbnailPath(file: FileInfoEntity) {
    return `${environment.serverUrl}${file.links.thumb}`;
  }

  public openFileModal(file: FileInfoEntity) {
    const initialState = {
      file,
    };
    this.bsModalRef = this.modalService.show(FileComponent, {initialState, class: 'modal-lg'});
  }

  /**
   * Fetch the directories and files for the specified directory
   */
  protected loadDirectory() {

    this.activatedRoute.params.subscribe((params) => {

      this.files = [];
      this.directories = [];

      this.currentPath = params.path ? params.path : '';
      this.breadcrumbs = params.path ? params.path.split('/') : [];

      this.fileService
        .getByPath(params.path)
        .pipe(finalize(() => {

        }))
        .subscribe(
          (response) => {
            if (response.data) {

              // Map and set the files available to the template
              if (Array.isArray(response.data.files)) {
                response.data.files.forEach((app) => {
                  this.files.push(new FileInfoEntity().populateFromObject(app));
                });
              }

              // Set the directories available to the template
              if (Array.isArray(response.data.dirs)) {
                this.directories = response.data.dirs;
              }
            }
          },
          (error) => {
            console.log(error);
          }
        );
    });
  }
}

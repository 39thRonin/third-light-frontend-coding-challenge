import { Component, OnInit } from '@angular/core';
import { FileKind } from 'src/app/enums/file-kind.enum';
import { FileInfoEntity } from 'src/app/classes/file/info-entity.class';
import { environment } from '../../../environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileService } from 'src/app/services/file.service';
import { finalize } from 'rxjs/operators';

/**
 * The file component
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'modal-content',
  templateUrl: './file.component.html',
})
export class FileComponent implements OnInit {

  public metadataForms: FormGroup[] = [];

  public metadataArrayForms: FormGroup[] = [];

  /**
   * Allow the FileKind enum to be accessible to the template
   */
  public fileKind = FileKind;

  public file: FileInfoEntity;

  public editDescription = false;
  public editCustomTags = false;

  public editMetadataField: string = null;

  public constructor(
    private formBuilder: FormBuilder,
    private fileService: FileService,
  ) {

  }

  public ngOnInit() {

    this.file.metadataFields.forEach((field) => {
      this.metadataForms.push(this.formBuilder.group({
        input: [this.file.metadata[field]],
      }));
    });

    this.file.metadataArrayFields.forEach((field) => {
      this.metadataArrayForms.push(this.formBuilder.group({
        input: [this.file.metadata[field]],
      }));
    });
  }

  public getThumbPath() {
    return `${environment.serverUrl}${this.file.links.thumb}`;
  }

  public getPreviewPath() {
    return `${environment.serverUrl}${this.file.links.preview}`;
  }

  public getDownloadPath() {
    return `${environment.serverUrl}/file/${this.file.path}`;
  }

  public onClickMetadataField(metadataField: string) {
    this.editMetadataField = metadataField ;
  }

  public onClickSaveMetadataField(metadataField: string, form: FormGroup) {
    this.file.metadata[metadataField] = form.get('input').value;
    this.editMetadataField = null;
    this.saveMetadata();
  }

  public onClickDismissMetadataField(metadataField: string, form: FormGroup) {
    this.editMetadataField = null;
    form.get('input').setValue(this.file.metadata[metadataField]);
  }

  public onClickRemoveFromMetadataField(metadataArrayField, i) {
    this.file.metadata[metadataArrayField].splice(i, 1);
    this.saveMetadata();
  }

  public onClickSaveMetadataArrayField(metadataArrayField: string, form: FormGroup) {
    const tags: string[] = [];
    form.get('input').value.split(',').forEach((tag: string) => {
      const value = tag.trim();
      if (value.length > 0) {
        tags.push(value);
      }
    });
    this.file.metadata[metadataArrayField] = tags;
    this.editMetadataField = null;
    this.saveMetadata();
  }

  onClickDismissMetadataArrayField(metadataArrayField: string, form: FormGroup) {
    this.editMetadataField = null;
    form.get('tags').setValue(this.file.metadata[metadataArrayField]);
  }

  private saveMetadata() {
    this.fileService.updateMetaDataByFile(this.file)
    .pipe(finalize(() => {

    }))
    .subscribe(
      (response) => {
        // console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}

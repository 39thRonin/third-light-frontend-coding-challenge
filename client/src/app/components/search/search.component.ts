import { Component, OnInit } from '@angular/core';
import { FileService } from 'src/app/services/file.service';
import { finalize } from 'rxjs/operators';
import { FileInfoEntity } from 'src/app/classes/file/info-entity.class';
import { FileKind } from 'src/app/enums/file-kind.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FileComponent } from '../file/file.component';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

/**
 * The directory component
 */
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {

  /**
   * The search form
   */
  public searchForm: FormGroup = this.formBuilder.group({
    search: ['', Validators.maxLength(255) ],
  });

  /**
   * The files to show
   */
  public files: FileInfoEntity[] = [];

  public query: string = null;

  /**
   * Allow the FileKind enum to be accessible to the template
   */
  public fileKind = FileKind;

  private bsModalRef: BsModalRef;

  /**
   * Constructor
   */
  public constructor(
    private fileService: FileService,
    private activatedRoute: ActivatedRoute,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) {}

  /**
   * Actions to perform AfterViewInit
   */
  public ngOnInit(): void {
    this.loadResults();
  }

  public getFileDownloadPath(file: FileInfoEntity) {
    return `${environment.serverUrl}${file.links.original}`;
  }

  public getThumbnailPath(file: FileInfoEntity) {
    return `${environment.serverUrl}${file.links.thumb}`;
  }

  public openFileModal(file: FileInfoEntity) {
    const initialState = {
      file,
    };
    this.bsModalRef = this.modalService.show(FileComponent, {initialState, class: 'modal-lg'});
  }

  public onSubmitSearchForm() {
    this.router.navigate(['/search/' + this.searchForm.get('search').value]);
  }

  /**
   * Fetch the directories and files for the specified directory
   */
  protected loadResults() {

    this.activatedRoute.params.subscribe((params) => {

      this.files = [];
      this.query = params.query;

      this.fileService
        .getByQuery(params.query)
        .pipe(finalize(() => {

        }))
        .subscribe(
          (response) => {
            if (response.data) {

              // Map and set the files available to the template
              if (Array.isArray(response.data)) {
                response.data.forEach((app) => {
                  this.files.push(new FileInfoEntity().populateFromObject(app));
                });
              }
            }
          },
          (error) => {
            console.log(error);
          }
        );
    });
  }
}

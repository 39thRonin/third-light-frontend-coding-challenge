import { Directive, Input, HostListener } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector : '[href]',
})
export class HrefDirective {
  @Input() public href: string;

  @HostListener('click', ['$event'])
  private onClick($event) {
    if (this.href.length === 0 || this.href === '#') {
      $event.preventDefault();
    }
  }
}

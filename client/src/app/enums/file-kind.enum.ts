export enum FileKind {
  Image = 'image',
  Video = 'video',
  Audio = 'audio',
  Application = 'application',
}

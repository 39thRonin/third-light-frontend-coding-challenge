export enum ServerResponseStatus {
  Okay = 'OK',
  Error = 'ERROR',
}

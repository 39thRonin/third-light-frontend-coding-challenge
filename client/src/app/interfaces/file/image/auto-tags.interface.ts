/**
 * The file image auto tags interface
 */
export interface FileImageAutoTags {
  color: string;
  tags: string[];
}

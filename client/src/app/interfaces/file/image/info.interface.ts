import { FileImageAutoTags } from './auto-tags.interface';
import { FileImageLocation } from './location.interface';

/**
 * The file image info interface
 */
export interface FileImageInfo {
  width: number;
  height: number;
  created: string;
  location: FileImageLocation;
  autotags: FileImageAutoTags;
}

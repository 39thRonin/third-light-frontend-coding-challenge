/**
 * The file image location interface
 */
export interface FileImageLocation {
  lat: number;
  long: number;
}

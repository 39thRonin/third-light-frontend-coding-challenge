import { FileKind } from 'src/app/enums/file-kind.enum';
import { FileImageInfo } from './image/info.interface';
import { FileLinks } from './links.interface';

/**
 * The file info interface
 */
export interface FileInfo {
  name: string;
  size: number;
  modTime: string;
  kind: FileKind;
  filetype: string;
  imageInfo: FileImageInfo;
  exif: object;
  metadata: object;
  path: string;
  links: FileLinks;
}

/**
 * The file links interface
 */
export interface FileLinks {
  dir: string;
  thumb: string;
  preview: string;
  original: string;
  metadata: string;
}

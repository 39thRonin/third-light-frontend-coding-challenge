import { FileInfo } from '../../file/info.interface';

/**
 * The server directory response data interface
 */
export interface ServerDirectoryResponseData {
  dirs: string[];
  files: FileInfo[];
}

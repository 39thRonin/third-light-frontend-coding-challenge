import { ServerDirectoryResponseData } from './response-data.interface';
import { ServerResponseStatus } from 'src/app/enums/server-response-status.enum';

/**
 * The server directory response interface
 */
export interface ServerDirectoryResponse {
  status: ServerResponseStatus;
  data?: ServerDirectoryResponseData;
  error?: string;
}

import { ServerResponseStatus } from 'src/app/enums/server-response-status.enum';

/**
 * The server metadata response interface
 */
export interface ServerMetadataResponse {
  status: ServerResponseStatus;
  data?: object;
  error?: string;
}

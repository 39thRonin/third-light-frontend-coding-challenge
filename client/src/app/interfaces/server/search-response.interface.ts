import { ServerResponseStatus } from 'src/app/enums/server-response-status.enum';
import { FileInfo } from '../file/info.interface';

/**
 * The server directory response interface
 */
export interface ServerSearchResponse {
  status: ServerResponseStatus;
  data?: FileInfo[];
  error?: string;
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'niceDate'})
export class NiceDatePipe implements PipeTransform {

  private weekDays: string[] = [
    'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
  ];

  private months: string[] = [
    'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',
  ];

  public transform(value: string): string {

    if (!value) {
      return;
    }

    // Set the date from the value provided
    const date = new Date(value);

    // Work out the date from a week ago today
    const weekAgo = new Date();
    weekAgo.setDate(weekAgo.getDate() - 7);

    // Format the minutes to use a leading zero
    const minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();

    // Display different formats depending on how recent
    if (weekAgo.getTime() < date.getTime()) {
      return `${this.weekDays[date.getDay()]} ${date.getHours()}:${minutes}`;
    }
    return `${date.getDate()} ${this.months[date.getMonth()]} ${date.getFullYear()}`;
  }
}

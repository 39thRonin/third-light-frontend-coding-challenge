import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ServerDirectoryResponse } from '../interfaces/server/directory/response.interface';
import { ServerResponseStatus } from '../enums/server-response-status.enum';
import { FileInfoEntity } from '../classes/file/info-entity.class';
import { ServerMetadataResponse } from '../interfaces/server/metadata-response.interface';
import { ServerSearchResponse } from '../interfaces/server/search-response.interface';

@Injectable()
export class FileService {

  public constructor(private http: HttpClient) {}

  public getByPath(path: string = ''): Observable<ServerDirectoryResponse> {
    return this.http
      .get<ServerDirectoryResponse>(
        `${environment.serverUrl}/dir/${path}`
      ).pipe(
        map((response) => {
          if (response.status !== ServerResponseStatus.Okay) {
            throwError(response.error ? response.error : 'Unsuccessful response');
          }
          return response;
        })
      );
  }

  public getByQuery(query: string = ''): Observable<ServerSearchResponse> {
    return this.http
      .get<ServerSearchResponse>(
        `${environment.serverUrl}/search/?q=${query}`
      ).pipe(
        map((response) => {
          if (response.status !== ServerResponseStatus.Okay) {
            throwError(response.error ? response.error : 'Unsuccessful response');
          }
          return response;
        })
      );
  }

  public updateMetaDataByFile(file: FileInfoEntity): Observable<ServerMetadataResponse> {
    return this.http
      .post<ServerMetadataResponse>(
        `${environment.serverUrl}${file.links.metadata}`,
        JSON.stringify(file.metadata),
      ).pipe(
        map((response) => {
          if (response.status !== ServerResponseStatus.Okay) {
            throwError(response.error ? response.error : 'Unsuccessful response');
          }
          return response;
        })
      );
  }
}
